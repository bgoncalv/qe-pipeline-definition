#!/usr/bin/bash

# shellcheck disable=SC2317  # Don't warn about unreachable commands in this file

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'bkr_job_submitter'
    cleanup() {
        rm -rf test_variables.env
        rm -f job_J*.xml
    }
    AfterEach 'cleanup'

    export DW_CHECKOUT="mock-checkout"
    export TEST_JOB_NAME="mock-testjob"
    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export TEST_PLAN_NAME="mock-testplan testplan-param"
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export JOB_SUBMITTER_PARMS='--job-owner=test $(cmd)'
    export TEST_COMPOSE=mock-bkr-compose
    export TEST_COMPOSE_TAGS="mock-tag1 mock-tag2"
    export bkr_output="Name: mocked_compose"


    Mock cmd
        echo "mycmd"
    End

    Mock bkr
        if [[ "$1" == "distros-list" ]]; then
            echo "${bkr_output}"
        elif [[ "$1" == "job-results" ]]; then
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
        else
            echo "bkr $*"
        fi
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0-mock.el10"
    End

    Mock JobSubmitter.sh
        if [[ ${BUILD:?} != "mocked_compose" ]]; then
            echo "JobSubmitter.sh expects BUILD variable to be set to beaker compose"
            exit 1
        fi
        if [[ -n "${BUILD_REPO:-}" ]]; then
            # JobSubmitter adds this parameter if BUILD_REPO is defined
            echo "Submitted with: --repo-post ${BUILD_REPO}"
        fi
        if [[ -n "${BUILD_MR_URL:-}" ]]; then
            # lib_taskfile, a JobSubmitter dependency, implements this logic if BUILD_MR_URL is defined
            mr_number="${BUILD_MR_URL##*/}"
            echo "Added to whiteboard: [MR-${mr_number}](${BUILD_MR_URL})"
        fi
        echo "Successfully submitted as TJ#9400840"
        echo "Successfully submitted as TJ#9400841"
        echo "Successfully submitted as TJ#9400842"
    End

    Mock beaker-jobwatch
        echo "toggle_nacks_on=on&job_ids=9400840+9400841"
        echo "toggle_nacks_on=on&job_ids=9400840+9400841+9400842"
        echo "toggle_nacks_on=on&job_ids=9400840+9400841+9400842+9400843"
    End

    Mock kcidb_tool
        echo "kcidb_tool ${*}"
    End

    Mock result2osci
        echo "result2osci ${*}"
    End

    It 'can submit job'
        When run script scripts/bkr_job_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stderr should include "bkr distros-list --name mock-bkr-compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The stderr should include "JobSubmitter.sh mock-testplan testplan-param --job-owner=test mycmd --arch x86_64 --nvr kernel-debug-5.14.0-mock.el10"
        The stderr should include "bkr job-results --prettyxml J:9400840"
        The stderr should include "bkr job-results --prettyxml J:9400841"
        The stderr should include "bkr job-results --prettyxml J:9400842"
        The contents line 1 of file test_variables.env should equal "BEAKER_JOBIDS=J:9400840 J:9400841 J:9400842 J:9400843"
        The contents line 2 of file test_variables.env should equal "TEST_PACKAGE_NAME=kernel-debug"
        The contents line 3 of file test_variables.env should equal "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents line 4 of file test_variables.env should equal "TEST_PACKAGE_NVR=kernel-debug-5.14.0-mock.el10"
        The contents line 5 of file test_variables.env should equal "TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-mock.el10"
        The contents line 6 of file test_variables.env should equal "TEST_ARCH=x86_64"
    End

    It 'can submit job with TEST_PACKAGE_IS_NOARCH'
        export TEST_PACKAGE_IS_NOARCH=true
        When run script scripts/bkr_job_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stderr should include "bkr distros-list --name mock-bkr-compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The stderr should include "JobSubmitter.sh mock-testplan testplan-param --job-owner=test mycmd --arch x86_64 --nvr kernel-debug-5.14.0-mock.el10.noarch"
        The stderr should include "bkr job-results --prettyxml J:9400840"
        The stderr should include "bkr job-results --prettyxml J:9400841"
        The stderr should include "bkr job-results --prettyxml J:9400842"
        The contents line 1 of file test_variables.env should equal "BEAKER_JOBIDS=J:9400840 J:9400841 J:9400842 J:9400843"
        The contents line 2 of file test_variables.env should equal "TEST_PACKAGE_NAME=kernel-debug"
        The contents line 3 of file test_variables.env should equal "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents line 4 of file test_variables.env should equal "TEST_PACKAGE_NVR=kernel-debug-5.14.0-mock.el10"
        The contents line 5 of file test_variables.env should equal "TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-mock.el10"
        The contents line 6 of file test_variables.env should equal "TEST_ARCH=x86_64"
    End

    It 'can submit job and report to OSCI if there is package nvr'
        export KOJI_SERVER="test-koji"
        export CI_PIPELINE_URL="pipeline.test/"
        export CI_PIPELINE_ID="123456"
        export REPORT_OSCI=1
        export TEST_ARCH="x86_64"
        export TEST_PACKAGE_NAME="kernel"
        export TEST_SOURCE_PACKAGE_NVR="kernel-5.14.0-496.el9"
        When run script scripts/bkr_job_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stderr should include "result2osci --source-nvr kernel-5.14.0-496.el9 --certificate /tmp/umb_certificate.pem --pipeline-id 123456 --log-url pipeline.test/ --test-status running --run-url pipeline.test/ --test-namespace kernel-qe.kernel-ci.kernel-debug-x86_64"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:9400840 J:9400841 J:9400842 J:9400843"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-496.el9"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job and NOT report to OSCI if package nvr is NOT set'
        export KOJI_SERVER="test-koji"
        export CI_PIPELINE_URL="pipeline.test/"
        export REPORT_OSCI=1
        When run script scripts/bkr_job_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stderr should not include "result2osci"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:9400840 J:9400841 J:9400842 J:9400843"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_PACKAGE_NVR=kernel-debug-5.14.0-mock.el10"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-mock.el10"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job and handle cancelled due to timeout'
        Mock beaker-jobwatch
            echo "/matrix/?toggle_nacks_on=on&job_ids=12345+12346"
            exit 124
        End
        export KOJI_SERVER="test-koji"
        export CI_PIPELINE_URL="pipeline.test/"
        When run script scripts/bkr_job_submitter.sh
        The status should be success
        The stdout should include "WARN: tests took over 24 hours to run, give up waiting for completion..."
        The stderr should include "bkr job-cancel J:12345 J:12346"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:12345 J:12346"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_PACKAGE_NVR=kernel-debug-5.14.0-mock.el10"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-mock.el10"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job for a CKI MR trigger'
        export BUILD_REPOS='[{"name":"kernel", "arch":"x86_64", "url":"https://example/publish_x86_64/"},
                             {"name":"kernel-debug", "arch":"x86_64", "url":"https://example/publish_x86_64_debug/"},
                             {"name":"kernel", "arch":"aarch64", "url":"https://example/publish_aarch64/"},
                             {"name":"kernel-debug", "arch":"aarch64", "url":"https://example/publish_aarch64_debug/"}]'
        export BUILD_MR_URL="https://example/merge_requests/1"
        export TEST_PACKAGE_NAME_ARCH="kernel-aarch64"
        export TEST_SOURCE_PACKAGE_NVR="kernel-5.14.0-mock.1_2.el10"
        When run script scripts/bkr_job_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stderr should include "bkr distros-list --name mock-bkr-compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The stderr should include "JobSubmitter.sh mock-testplan testplan-param --job-owner=test mycmd --arch aarch64 --nvr kernel-5.14.0-mock.1_2.el10"
        The stderr should include "bkr job-results --prettyxml J:9400840"
        The stderr should include "bkr job-results --prettyxml J:9400841"
        The stderr should include "bkr job-results --prettyxml J:9400842"
        The stdout should include "--repo-post https://example/publish_aarch64/"
        The stdout should include "[MR-1](${BUILD_MR_URL})"
        The contents line 1 of file test_variables.env should equal "BEAKER_JOBIDS=J:9400840 J:9400841 J:9400842 J:9400843"
        The contents line 2 of file test_variables.env should equal "TEST_PACKAGE_NAME=kernel"
        The contents line 3 of file test_variables.env should equal "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents line 4 of file test_variables.env should equal "TEST_PACKAGE_NVR=kernel-5.14.0-mock.1_2.el10"
        The contents line 5 of file test_variables.env should equal "TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-mock.1_2.el10"
        The contents line 6 of file test_variables.env should equal "TEST_ARCH=aarch64"
    End
End
