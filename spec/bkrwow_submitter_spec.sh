#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'bkrwow_submitter'
    cleanup() {
        rm -rf test_variables.env
    }
    AfterEach 'cleanup'

    export DW_CHECKOUT="mock-checkout"
    export TEST_JOB_NAME="mock-testjob"
    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export WOW_TASKFILE=mock-taskfile
    export JOB_OWNER=test
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export WOW_WHITEBOARD='test $(cmd)'
    export TEST_COMPOSE="mock_compose"
    export TEST_COMPOSE_TAGS='mock-tag1 mock-tag2'
    export bkr_distro_output="Name: mocked_compose"

    Mock cmd
        echo "mycmd"
    End

    Mock bkr
        if [[ "$1" == "distros-list" ]]; then
            echo "${bkr_distro_output}"
        elif [[ "$1" == "workflow-tomorrow" ]]; then
            echo "Successfully submitted as TJ#123456"
            echo "Successfully submitted as TJ#123457"
            echo "Successfully submitted as TJ#123458"
        elif [[ "$1" == "job-results" ]]; then
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
        else
            echo "fail, unsupported bkr command $1"
            exit 1
        fi
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0-mock.el10"
    End

    Mock beaker-jobwatch
        echo "toggle_nacks_on=on&job_ids=123456+123457+123458"
    End

    Mock kcidb_tool
        echo "kcidb_tool ${*}"
    End

    It 'can submit job'
        When run script scripts/bkrwow_submitter.sh
        The status should be success
        The stdout should include "${bkr_distro_output}"
        The stderr should include "bkr distros-list --name mock_compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The stderr should include "bkr workflow-tomorrow --distro mocked_compose --arch x86_64 --ks-meta=redhat_ca_cert --exact --task-file mock-taskfile --job-owner=test --whiteboard 'test mycmd - x86_64'"
        The stderr should include "wait_jobs"
        The stderr should include "timeout 24h beaker-jobwatch --queue-timeout 43200 --job J:123456,J:123457,J:123458"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456 J:123457 J:123458"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
End
