#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'stbkr_submitter'
    startup() {
        mkdir -p kernel/storage/misc/toolbox
    }
    cleanup() {
        rm -rf test_variables.env
        rm -rf kernel
    }
    Before 'startup'
    AfterEach 'cleanup'

    export DW_CHECKOUT="mock-checkout"
    export TEST_JOB_NAME="mock-testjob"
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export STBKR_TESTS_CONF_REPO="mock_repo"
    export TEST_COMPOSE="mock_compose"
    export TEST_COMPOSE_TAGS="mock_tag"
    export CONFIG="test_config"
    export STBKR_GROUP="test_group"
    export JOB_OWNER=test

    export stbkr_output="J:123456"

    Mock git
        echo "git $*"
    End

    Mock stbkr
        echo "${stbkr_output}"
    End

    Mock bkr
        if [[ "$1" == "job-results" ]]; then
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
        else
            echo "fail, unsupported bkr command $1"
            exit 1
        fi
    End

    Mock beaker-jobwatch
        echo "toggle_nacks_on=on&job_ids=123456"
    End

    Mock kcidb_tool
        echo "kcidb_tool ${*}"
    End

    It 'can submit job kernel'
        export TEST_PACKAGE_NAME_ARCH=kernel-x86_64
        When run script scripts/stbkr_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mock_repo kernel"
        The stderr should include "stbkr --default-kernel 1 --debug-kernel 0 --rt-kernel 0 --64k-kernel 0 --dist_tag mock_tag --group test_group -r mock_compose -c test_conf/test_config -a x86_64 --job-owner test"
        The stderr should include "wait_job"
        The stderr should include "timeout 24h beaker-jobwatch --queue-timeout 43200 --job J:123456 --job-owner test"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job kernel-debug'
        export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
        When run script scripts/stbkr_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mock_repo kernel"
        The stderr should include "stbkr --default-kernel 0 --debug-kernel 1 --rt-kernel 0 --64k-kernel 0 --dist_tag mock_tag --group test_group -r mock_compose -c test_conf/test_config -a x86_64 --job-owner test"
        The stderr should include "wait_job"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job kernel-rt'
        export TEST_PACKAGE_NAME_ARCH=kernel-rt-x86_64
        When run script scripts/stbkr_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mock_repo kernel"
        The stderr should include "stbkr --default-kernel 0 --debug-kernel 0 --rt-kernel 1 --64k-kernel 0 --kernel_ops efi=runtime --dist_tag mock_tag --group test_group -r mock_compose -c test_conf/test_config -a x86_64 --job-owner test"
        The stderr should include "wait_job"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-rt"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job kernel-64k'
        export TEST_PACKAGE_NAME_ARCH=kernel-64k-x86_64
        When run script scripts/stbkr_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mock_repo kernel"
        The stderr should include "stbkr --default-kernel 0 --debug-kernel 0 --rt-kernel 0 --64k-kernel 1 --dist_tag mock_tag --group test_group -r mock_compose -c test_conf/test_config -a x86_64 --job-owner test"
        The stderr should include "wait_job"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-64k"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job kernel-rt-5.14.0-427.28.1.el9_4'
        export TEST_PACKAGE_NAME_ARCH=kernel-rt-x86_64
        export TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-427.28.1.el9_4
        When run script scripts/stbkr_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mock_repo kernel"
        The stderr should include "stbkr --kernel kernel-rt-5.14.0-427.28.1.el9_4 --kernel_ops efi=runtime --default-kernel 0 --debug-kernel 0 --rt-kernel 1 --64k-kernel 0 --kernel_ops efi=runtime --dist_tag mock_tag --group test_group -r mock_compose -c test_conf/test_config -a x86_64 --job-owner test"
        The stderr should include "wait_job"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-rt"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job iscsi-initiator-utils-6.2.1.9-1.gita65a472.el9'
        export TEST_SOURCE_PACKAGE_NAME=iscsi-initiator-utils
        export TEST_PACKAGE_NAME_ARCH=iscsi-initiator-utils-x86_64
        export TEST_SOURCE_PACKAGE_NVR=iscsi-initiator-utils-6.2.1.9-1.gita65a472.el9
        When run script scripts/stbkr_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mock_repo kernel"
        The stderr should include "stbkr --pkg iscsi-initiator-utils-6.2.1.9-1.gita65a472.el9 --default-kernel 1 --debug-kernel 0 --rt-kernel 0 --64k-kernel 0 --dist_tag mock_tag --group test_group -r mock_compose -c test_conf/test_config -a x86_64 --job-owner test"
        The stderr should include "wait_job"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=iscsi-initiator-utils"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=iscsi-initiator-utils"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It "can't submit job if has multiple tags"
        export TEST_PACKAGE_NAME_ARCH=kernel-x86_64
        export TEST_COMPOSE_TAGS="mock_tag1 mock_tag2"
        When run script scripts/stbkr_submitter.sh
        The status should be failure
        The stdout should include "git clone --depth 1 mock_repo kernel"
        The stderr should include "FAIL: TEST_COMPOSE_TAGS (mock_tag1 mock_tag2) has to be 1 word"
    End
End
