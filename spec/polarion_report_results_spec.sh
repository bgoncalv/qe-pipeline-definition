#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'polarion_report_results'
    startup() {
        mkdir -p kernel-general/FeedPolarion
    }
    BeforeEach 'startup'

    export KERNEL_GENERAL_INTERNAL_REPO=test-repo
    export POLARION_OWNER=polarion_owner
    export POLARION_TEST_PHASE=test_phase_1
    export POLARION_RUN_TITLE=mock_test_run
    export POLARION_CONFIG_MAP_FILE=mock_config
    # BEAKER_JOBIDS is needed when calling polarion_report_results.sh
    export BEAKER_JOBIDS="J:12345 J:123455"
    export POLARION_USER=polarion_user
    export POLARION_PASSWORD=polarion_password
    export POLARION_URL=https://polarion.test

    Mock git
        echo "git $*"
    End

    Mock python3
        echo python3 "$*"
    End

    It 'can report results'
        When run script scripts/polarion_report_results.sh
        The status should be success
        The stdout should include "INFO: generating polarion xunit results and upload it."
        The stderr should include "pushd ./FeedPolarion"
        The stderr should include "python3 main.py --polarion-custom-assignee polarion_owner -u polarion_user --passwd polarion_password --planned-in test_phase_1 --testrun-title mock_test_run --config-map-json mock_config --output /tmp/testrun.xml --submit -j 12345 -j 123455"
        The stderr should include "popd"
    End
End
