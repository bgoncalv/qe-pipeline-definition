#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'triager'
    startup() {
        echo "{ \"checkouts\": [{ \"id\": \"specrun:${DW_CHECKOUT_ID}\" }], " > ./output.json
        echo " \"builds\": [{ \"id\": \"specrun:${DW_BUILD_ID}\" }] }" >> ./output.json
    }

    cleanup() {
        rm -rf ./output.json
        rm -f output_triaged.json
        rm -f slack_message.txt
    }
    Before 'startup'
    After 'cleanup'

    Mock jinja_renderer
    End

    Mock python3
        # create a valid json
        echo "{\"python3\": \"$2\"}"
    End

    Mock result2osci
        echo "result2osci ${*}"
    End

    export CI_PIPELINE_URL="pipeline.test/"
    export CI_PIPELINE_ID="123456"
    export DW_CHECKOUT_ID="mocked_dw_checkout_id"
    export DW_BUILD_ID="${DW_CHECKOUT_ID}-build"
    export KOJI_SERVER="test-koji"
    export TEST_ARCH="x86_64"
    export TEST_PACKAGE_NAME="kernel"
    export TEST_SOURCE_PACKAGE_NVR="kernel-5.14.0-496.el9"

    It 'can triage'
        echo "@test please review" > slack_message.txt
        When run script scripts/triager.sh
        The status should be success
        The stdout should include "@test please review"
        The stderr should include "python3 -m reporter -c specrun:mocked_dw_checkout_id -b specrun:mocked_dw_checkout_id-build -t failure-json"
        The stderr should include "TESTRUN_LOG_URL=https://datawarehouse.cki-project.org/kcidb/builds/specrun:${DW_BUILD_ID}"
        The stderr should include "jinja_renderer -t scripts/notify_slack.j2 -i reporter.json -o slack_message.txt"
    End

    It 'can send message to OSCI'
        export REPORT_OSCI=1
        echo "@test please review" > slack_message.txt
        When run script scripts/triager.sh
        The status should be success
        The stdout should include "@test please review"
        The stderr should include "result2osci --source-nvr kernel-5.14.0-496.el9 --certificate /tmp/umb_certificate.pem --pipeline-id 123456 --log-url pipeline.test/ --test-status complete --run-url https://datawarehouse.cki-project.org/kcidb/builds/specrun:${DW_BUILD_ID} --test-namespace kernel-qe.kernel-ci.kernel-x86_64 --test-result failed"
    End

    It 'can send failed message to OSCI with OSCI_GATE_ONLY_FAIL'
        export OSCI_GATE_ONLY_FAIL=1
        export REPORT_OSCI=1
        echo "Failed: 2 | Errored: 16" > slack_message.txt
        echo "@test please review" >> slack_message.txt
        When run script scripts/triager.sh
        The status should be success
        The stdout should include "@test please review"
        The stderr should include "result2osci --source-nvr kernel-5.14.0-496.el9 --certificate /tmp/umb_certificate.pem --pipeline-id 123456 --log-url pipeline.test/ --test-status complete --run-url https://datawarehouse.cki-project.org/kcidb/builds/specrun:${DW_BUILD_ID} --test-namespace kernel-qe.kernel-ci.kernel-x86_64 --test-result failed"
    End

    It 'can send info message to OSCI with OSCI_GATE_ONLY_FAIL'
        export OSCI_GATE_ONLY_FAIL=1
        export REPORT_OSCI=1
        echo "Errored: 16" > slack_message.txt
        echo "@test please review" >> slack_message.txt
        When run script scripts/triager.sh
        The status should be success
        The stdout should include "@test please review"
        The stderr should include "result2osci --source-nvr kernel-5.14.0-496.el9 --certificate /tmp/umb_certificate.pem --pipeline-id 123456 --log-url pipeline.test/ --test-status complete --run-url https://datawarehouse.cki-project.org/kcidb/builds/specrun:${DW_BUILD_ID} --test-namespace kernel-qe.kernel-ci.kernel-x86_64 --test-result info"
    End
End
