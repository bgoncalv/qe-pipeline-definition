#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'bkr_zsubmit_submitter'
    cleanup() {
        rm -f test_variables.env
        rm -f bkr_ids.txt
    }
    AfterEach 'cleanup'

    export DW_CHECKOUT="mock-checkout"
    export TEST_JOB_NAME="mock-testjob"
    export TEST_PACKAGE_NAME_ARCH=kernel-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export TEST_COMPOSE="mocked_compose"
    export TEST_COMPOSE_TAGS="mock-tag1 mock-tag2"
    export KERNEL_GENERAL_INTERNAL_REPO=mock-repo
    export ZSUBMIT_TIER='KT1 KT2 REG'
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export ZSUBMIT_PARAMS='--noduplicate --whiteboard $(cmd)'
    export bkr_output="Name: mocked_compose"
    export KERNEL_GENERAL_BOT_KEYTAB="mock_keytab"
    export KERNEL_GENERAL_BOT_NAME_REALM="mock_name_realm"

    Mock base64
        echo "mock_decoded_keytab"
    End

    Mock beaker-jobwatch
        echo "toggle_nacks_on=on&job_ids=123456"
    End

    Mock bkr
        if [[ "$1" == "distros-list" ]]; then
            echo "${bkr_output}"
        elif [[ "$1" == "job-results" ]]; then
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
            echo 'status="Completed"'
            echo 'distro="mocked-distro"'
        else
            echo "bkr $*"
        fi
    End

    Mock cmd
        echo "mycmd"
    End

    Mock dnf
        echo "dnf $*"
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0-mock.el10"
    End

    Mock git
        echo "git $*"
    End

    Mock kcidb_tool
        echo "kcidb_tool ${*}"
    End

    Mock kinit
        echo "kinit $*"
    End

    Mock ZErrata
        echo "kernel-5.14.0-mock.el10"
    End

    Mock ZSubmit
        echo -n "J:123456" >> bkr_ids.txt
    End

    It 'can submit job with ZSubmit - ZSUBMIT_TIER and ZSUBMIT_PARAMS set'
        When run script scripts/bkr_zsubmit_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stderr should include "ZSubmit --noduplicate --whiteboard mycmd --nvr kernel-5.14.0-mock.el10 --arch x86_64 --tier KT1 KT2 REG --idf bkr_ids.txt"
        The stderr should include "bkr distros-list --name mocked_compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The stderr should include "bkr job-results --prettyxml J:123456"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_PACKAGE_NVR=kernel-5.14.0-mock.el10"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-mock.el10"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with ZSubmit - Errata trigger'
        export ERRATA_ID="000001"
        export TEST_PACKAGE_NAME_ARCH=kernel-rt-x86_64
        unset TEST_SOURCE_PACKAGE_NVR
        When run script scripts/bkr_zsubmit_submitter.sh
        The status should be success
        The stdout should include "Waiting for beaker jobs to finish: J:123456"
        The stderr should include "ZErrata info -e 000001 --build"
        The stderr should include "ZSubmit --noduplicate --whiteboard mycmd --nvr kernel-rt-5.14.0-mock.el10 --arch x86_64 --tier KT1 KT2 REG --idf bkr_ids.txt"
        The stderr should include "bkr job-results --prettyxml J:123456"
        The contents of file /tmp/kernel_general.keytab should include "mock_decoded_keytab"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-rt"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_PACKAGE_NVR=kernel-rt-5.14.0-mock.el10"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NVR=kernel-5.14.0-mock.el10"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
    It 'fail to submit job with debug package'
        export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
        When run script scripts/bkr_zsubmit_submitter.sh
        The status should be failure
        The stdout should include "FAIL: zsubmit jobs shouldn't trigger for debug packages"
        The stderr should include "FAIL: zsubmit jobs"
    End
End
