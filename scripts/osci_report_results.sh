#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

params=(--source-nvr "${TEST_SOURCE_PACKAGE_NVR:?}"
        --certificate /tmp/umb_certificate.pem
        --pipeline-id "${CI_PIPELINE_ID:?}"
        --log-url "${CI_PIPELINE_URL:?}"
        --test-status "${OSCI_TEST_STATUS:?}")

if [[ -z "${TESTRUN_LOG_URL:-}" ]]; then
    TESTRUN_LOG_URL="${CI_PIPELINE_URL}"
fi

params+=(--run-url "${TESTRUN_LOG_URL}")

if [[ -n "${OSCI_TEST_CATEGORY:-}" ]]; then
    params+=(--test-category "${OSCI_TEST_CATEGORY}")
fi

# as the pipeline can run for multiple variants and arches default
# namespace to be unique for each of them.
if [[ -z "${OSCI_TEST_NAMESPACE:-}" ]]; then
    OSCI_TEST_NAMESPACE="${TEST_PACKAGE_NAME:?}-${TEST_ARCH:?}"
fi
# all kernel-qe ci tests in OSCI should start with kernel-qe.kernel-ci
OSCI_TEST_NAMESPACE="kernel-qe.kernel-ci.${OSCI_TEST_NAMESPACE}"
params+=(--test-namespace "${OSCI_TEST_NAMESPACE}")

if [[ -n "${OSCI_TEST_TYPE:-}" ]]; then
    params+=(--test-type "${OSCI_TEST_TYPE}")
fi

if [[ "${OSCI_TEST_STATUS}" == "complete" ]]; then
    params+=(--test-result "${TEST_RESULT}")
fi

if [[ "${DRY_RUN:-}" == "1" ]]; then
    echo "dry run: it would run: result2osci ${params[*]}"
else
    echo "INFO: Submitting result to OSCI..."
    result2osci "${params[@]}"
fi

