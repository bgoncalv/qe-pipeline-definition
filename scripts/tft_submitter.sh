#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

if [[ -n "${TEST_PACKAGE_NAME_ARCH:-}" ]]; then
    # remove the / that are passed with TEST_PACKAGE_NAME
    TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
    # shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
    TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
fi
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
if [[ -z "${TEST_ARCH:-}" ]]; then
    TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
fi
TEST_JOB_TIMEOUT_HOURS=${TEST_JOB_TIMEOUT_HOURS:-12}
TEST_JOB_TIMEOUT_MINS=$(( TEST_JOB_TIMEOUT_HOURS*60 ))


tft_params=("--arch" "${TEST_ARCH}"
            # TMT might terminate some tests due to ssh timeout
            # this problem seems to happen when running debug kernels
            "--environment" "TMT_SSH_ConnectionAttempts=20"
            "--environment" "TMT_SSH_ServerAliveCountMax=180"
            "--git-url" "${TFT_PLAN_URL}"
            "--plan" "${TFT_PLAN}"
            "--timeout" "${TEST_JOB_TIMEOUT_MINS}")

if [[ -n "${TEST_SOURCE_PACKAGE_NAME:-}" ]]; then
    tft_params+=("--context" "component=${TEST_SOURCE_PACKAGE_NAME}")
fi

if [[ -n "${TFT_DISTRO:-}" ]]; then
    tft_params+=("--context" "distro=${TFT_DISTRO}")
fi

if [[ -n "${TEST_PACKAGE_NAME:-}" ]]; then
    tft_params+=("--context" "package-name=${TEST_PACKAGE_NAME}")
fi

if [[ -n "${TFT_STREAM:-}" ]]; then
    tft_params+=("--context" "stream=${TFT_STREAM}")
fi

if [[ -n "${TFT_DISCOVER:-}" ]]; then
    tft_params+=("--tmt-discover" "${TFT_DISCOVER}")
fi

if [[ -n "${TFT_TESTNAME:-}" ]]; then
    tft_params+=("--test" "${TFT_TESTNAME}")
fi

if [[ -n "${TEST_COMPOSE:-}" ]]; then
    if [[ -z "${AUTOMOTIVE_RELEASE:-}" ]]; then
        if [[ "${DRY_RUN:-}" != "1" ]]; then
            # we need to know the real compose that is going to be used as in some cases we will need to know the nvr
            # of a package in the compose under test.
            curl --retry 50 "${TFT_OSCI_STORAGE:?}/composes-production.json" > composes-production.json
            real_compose=$(jq -r '[."SYMBOLIC_COMPOSES"[] | select(."'"${TEST_COMPOSE}"'")][0] | ."'"${TEST_COMPOSE}"'"' < composes-production.json)

            # if not found, assume the compose was already a real one.
            if [[ "${real_compose}" != "null" ]]; then
                TEST_COMPOSE=${real_compose}
            fi
        fi
    fi
    tft_params+=("--compose" "${TEST_COMPOSE}")
# Try to get an automotive compose
elif [[ -n "${AUTOMOTIVE_WEBSERVER_RELEASES:-}" && -n "${AUTOMOTIVE_RELEASE}" ]]; then
    automotive_params=("--arch" "${TEST_ARCH}"
                       "--webserver-releases" "${AUTOMOTIVE_WEBSERVER_RELEASES}"
                       "--release" "${AUTOMOTIVE_RELEASE}")
    if [[ -n "${AUTOMOTIVE_HW_TARGET:-}" ]]; then
        automotive_params+=("--hw-target" "${AUTOMOTIVE_HW_TARGET}")
    fi
    if [[ -n "${AUTOMOTIVE_IMAGE_TYPE:-}" ]]; then
        automotive_params+=("--image-type" "${AUTOMOTIVE_IMAGE_TYPE}")
    fi
    if [[ -n "${AUTOMOTIVE_NAME:-}" ]]; then
        automotive_params+=("--image-name" "${AUTOMOTIVE_IMAGE_NAME}")
    fi
    echo "INFO: Getting automotive compose"
    TEST_COMPOSE=$(get_automotive_tf_compose "${automotive_params[@]}")
    if [[ -z "${TEST_COMPOSE}" ]]; then
        echo "FAIL: Couldn't get automotive compose."
        echo "FAIL: Automotive params:" "${automotive_params[@]}"
        exit 1
    else
        echo "INFO: Automotive compose ${TEST_COMPOSE} found."
    fi
    tft_params+=("--compose" "${TEST_COMPOSE}")
else
    echo "FAIL: No compose or automotive variables to get compose specified."
    exit 1
fi

if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}
    # only install package using redhat-brew-build if running for specific non kernel nvr
    # kernel NVR always use kernelinstal, even if installing rpm from the compose to be able to install for all variants
    if [[ ! "${TEST_SOURCE_PACKAGE_NVR}" =~ kernel ]]; then
        if [[ "${DRY_RUN:-}" == "1" ]]; then
            brew_task_id="123456"
        else
            brew_task_id=$(koji --noauth --server "${KOJI_SERVER:?}" call --json-output getBuild "${TEST_SOURCE_PACKAGE_NVR}" | jq .task_id)
        fi
        if [[ ! "${brew_task_id}" =~ ^[0-9]+$ ]]; then
            echo "FAIL: couldn't get brew task id of ${TEST_SOURCE_PACKAGE_NVR}."
            exit 1
        fi
        tft_params+=("--redhat-brew-build" "${brew_task_id}")
    fi
fi

# There is no need to query nvr package if it will not report results
if [[ -z "${TEST_SOURCE_PACKAGE_NVR:-}" ]] && [[ "${SKIP_REPORT_RESULTS:-}" != "1" ]]; then
    # still not supported to get nvr from automotive compose
    if [[ -n "${AUTOMOTIVE_RELEASE:-}" ]]; then
        TEST_SOURCE_PACKAGE_NVR="TODO"
    else
        TEST_SOURCE_PACKAGE_NVR=$(find_compose_pkg -c "${TEST_COMPOSE}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
        if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
            echo "FAIL: couldn't find nvr for ${TEST_SOURCE_PACKAGE_NAME} on compose ${TEST_COMPOSE}"
            exit 1
        fi
    fi
    TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}
fi

# kernel packages need to use kernelinstall, doesn't matter if triggered by specific nvr or
# from nvr part of the compose
if [[ "${TEST_PACKAGE_NAME}" =~ kernel ]]; then
    # debug is handled as variant...
    tft_params+=("--environment" "KERNELARGNAME=${TEST_PACKAGE_NAME/-debug/}")
    # shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
    kernel_vr=$(echo "${TEST_PACKAGE_NVR}" | sed 's/\(.*\)-\(.*-.*\)/\2/')
    tft_params+=("--environment" "KERNELARGVERSION=${kernel_vr}")
    if [[ "${TEST_PACKAGE_NAME}" =~ debug ]]; then
        tft_params+=("--environment" "KERNELARGVARIANT=debug")
    else
        tft_params+=("--environment" "KERNELARGVARIANT=up")
    fi
    tft_params+=("--environment" "KERNELARGEXTRAMODULES=1")
    tft_params+=("--environment" "KERNELARGINTERNALMODULES=1")
    tft_params+=("--tmt-discover" "--how=fmf --insert --url=https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git --test=/distribution/kernelinstall --order 10")
fi

if [[ -n "${TFT_CONTEXT:-}" ]]; then
    tft_params+=("--context" "${TFT_CONTEXT}")
fi

if [[ -n "${TFT_HARDWARE:-}" ]]; then
    IFS=';' read -r -a hw_array <<< "${TFT_HARDWARE}"
    for value in "${hw_array[@]}"; do
        # Split the value into the key and the condition to handle strings with embedded quote
        # first space character splits key and condition
        tft_params+=("--hardware" "${value/ /=}")
    done
fi

if [[ -n "${TFT_PARAMS:-}" ]]; then
    IFS=';' read -r -a param_array <<< "${TFT_PARAMS}"
    for param in "${param_array[@]}"; do
        for value in ${param}; do
            tft_params+=("${value}")
        done
    done
fi

if [[ -n "${TFT_POOL:-}" ]]; then
    tft_params+=("--pool" "${TFT_POOL}")
fi

if [[ "${REPORT_POLARION:-0}" -eq "1" ]]; then
    # https://docs.testing-farm.io/Testing%20Farm/0.1/test-request.html#polarion
    # https://tmt.readthedocs.io/en/stable/plugins/report.html#polarion
    tft_params+=("--tmt-environment" "POLARION_REPO=${POLARION_BASE_URL:?}/repo/")
    tft_params+=("--tmt-environment" "POLARION_URL=${POLARION_BASE_URL:?}/polarion/")
    tft_params+=("--tmt-environment" "POLARION_PROJECT=${POLARION_PROJECT:?}")
    tft_params+=("--tmt-environment" "POLARION_USERNAME=${POLARION_USER:?}")
    tft_params+=("--tmt-environment" "POLARION_PASSWORD=${POLARION_PASSWORD:?}")
    tft_params+=("--tmt-environment" "TMT_PLUGIN_REPORT_POLARION_PROJECT_ID=${POLARION_PROJECT:?}")
    tft_params+=("--tmt-environment" "TMT_PLUGIN_REPORT_POLARION_TITLE=${POLARION_RUN_TITLE:?}")
    tft_params+=("--tmt-environment" "TMT_PLUGIN_REPORT_POLARION_PLANNED_IN=${POLARION_TEST_PHASE:?}")
    tft_params+=("--tmt-environment" "TMT_PLUGIN_REPORT_POLARION_ASSIGNEE=${POLARION_OWNER:?}")
    tft_params+=("--context" "polarion_report=on")
fi

if [[ "${DRY_RUN:-}" == "1" ]]; then
    tft_params+=("--dry-run")
fi

testing-farm request --no-wait "${tft_params[@]}" | tee request_output.txt

if [[ "${DRY_RUN:-}" == "1" ]]; then
    echo "INFO: running as dry run, no job is created."
    echo "api https://api.dev.testing-farm.io/v0.1/requests/da7aa8a6-04c5-4e70-bbbc-cdeafe6bb732" | tee request_output.txt
fi
TFT_REQUEST_ID=$(grep "api" request_output.txt | grep -o '[^/]*$')
if [[ -z "${TFT_REQUEST_ID}" ]]; then
    echo "FAIL: Couldn't submit testing-farm request."
    exit 1
fi

{
    echo TFT_REQUEST_ID="${TFT_REQUEST_ID}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME:-}"
    echo TEST_PACKAGE_NVR="${TEST_PACKAGE_NVR:-}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:-}"
    echo TEST_SOURCE_PACKAGE_NVR="${TEST_SOURCE_PACKAGE_NVR:-}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env

# When testing composes, there are cases we don't know the NVR until
# testing-farm jobs finishes, only send "running" if we already know the NVR.
if [[ "${REPORT_OSCI:-}" == "1" ]] && [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    # these variables need to be exported as the script uses them
    export TEST_PACKAGE_NAME
    export TEST_ARCH
    # try to send the message, but don't fail the script if there is any problem.
    OSCI_TEST_STATUS=running scripts/osci_report_results.sh || true
fi

if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]] && [[ -n "${TEST_JOB_NAME:-}" ]]; then
    kcidb_tool create-test-plan --arch "${TEST_ARCH:?}" --src-nvr "${TEST_SOURCE_PACKAGE_NVR:?}" --nvr "${TEST_PACKAGE_NVR:?}" --checkout-origin "${TEST_JOB_NAME}" --builds-origin "${TEST_JOB_NAME:?}" -c "${DW_CHECKOUT:?}" -o test-plan.json
fi

# don't fail pipeline if a command fails
set +e
# wait for 24hrs for jobs to complete
wait_start=$(date +%s)
max_time=$(( TEST_JOB_TIMEOUT_HOURS*3600 ))

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

if [[ -f test-plan.json ]]; then
    kcidb_tool push2umb -i test-plan.json --certificate /tmp/umb_certificate.pem
    # Try to upload the test plan synchronously.
    # Failures are ignored as another round of uploading is handled external to
    # the pipeline via datawarehouse-submitter.
    if ! python3 -m cki_tools.datawarehouse_submitter --kcidb-file test-plan.json; then
        echo "FAIL: couldn't submit test-plans to DW. DW synchronous should handle it."
    fi
fi

# in case of failures connecting to beaker `bkr job-watch` might fail
# we should retry it and continue waiting for up to 24hrs since original
# start time.
# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
while [[ $(( $(date +%s) - max_time )) -lt "${wait_start}" ]]; do
    timeout "${TEST_JOB_TIMEOUT_HOURS}"h testing-farm watch --id "${TFT_REQUEST_ID}"
    if [[ $? == 124 ]]; then
        echo "FAIL: tests took over ${TEST_JOB_TIMEOUT_HOURS} hours to run, give up waiting for completion..."
        testing-farm cancel "${TFT_REQUEST_ID}"
        # wait for a bit for testing-farm to process the result to the canceled request.
        sleep 60
        break
    fi

    # make sure watch returned, because request completed and not due some infra problem
    curl --retry 50 "${TFT_API_URL:?}"/requests/"${TFT_REQUEST_ID}" > request_status.json
    state=$(jq -r ".state" < request_status.json)
    if [[ "${state}" == "complete" ]] ||
       [[ "${state}" == "error" ]]; then
        break
    fi
done
set -e
