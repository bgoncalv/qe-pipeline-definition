#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

. scripts/bkr_common.sh

set -x
# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
ARCH_PARAM=""
ARCH_PARAM="--arch ${TEST_ARCH}"
WOW_WHITEBOARD="${WOW_WHITEBOARD} - ${TEST_ARCH}"

bkr_get_compose
compose="${BKR_DISCOVERED_COMPOSE}"
if [[ -z "${compose}" ]]; then
    echo "FAIL: Couldn't query beaker compose."
    exit 1
fi
# don't fail pipeline if a command fails
set +e
if [[ -z "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    TEST_SOURCE_PACKAGE_NVR=$(find_compose_pkg -c "${compose}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
    if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
        echo "FAIL: couldn't find nvr for ${TEST_SOURCE_PACKAGE_NAME} on compose ${compose}"
        exit 1
    fi
fi
TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}

# TODO: how to install an specific kernel using wow?

# allow WOW_WHITEBOARD to contain command line execution
WOW_WHITEBOARD=$(eval echo "${WOW_WHITEBOARD}")

if [[ "${DRY_RUN:-}" == "1" ]]; then
    echo "INFO: running as dry run, no job is created."
    echo 'TJ#123456' | tee bkr_job.txt
else
    # shellcheck disable=SC2086 # Double quote to prevent globbing
    timeout 1m bkr workflow-tomorrow --distro "${compose}" ${ARCH_PARAM} --ks-meta=redhat_ca_cert --exact --task-file "${WOW_TASKFILE:?}" --job-owner="${JOB_OWNER:?}" --whiteboard "${WOW_WHITEBOARD}" 2>&1 | tee bkr_job.txt
    if [[ ${PIPESTATUS[0]} == 124 ]]; then
        echo "FAIL: timeout generating xml using workflow-tomorrow."
        exit 1
    fi
fi
BEAKER_JOBIDS=$(sed -n 's/.*TJ#\([0-9]\+\)$/J:\1/p' bkr_job.txt | tr '\n' ' ')
if [[ -z "${BEAKER_JOBIDS}" ]]; then
    echo "FAIL: Couldn't submit beaker job."
    exit 1
fi

wait_jobs
