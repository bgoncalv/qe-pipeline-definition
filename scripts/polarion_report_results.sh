#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

if [[ -z "${BEAKER_JOBIDS:-}" ]]; then
    echo "INFO: BEAKER_JOBIDS is not set"
    exit 0
fi

git clone --depth 1 "${KERNEL_GENERAL_INTERNAL_REPO:?}" kernel-general
cp -r kernel-general/FeedPolarion .
rm -rf kernel-general
pushd ./FeedPolarion || exit 1

outputfile=/tmp/testrun.xml

polarion_params=(--polarion-custom-assignee "${POLARION_OWNER:?}"
                 -u "${POLARION_USER:?}"
                 --passwd "${POLARION_PASSWORD:?}"
                 --planned-in "${POLARION_TEST_PHASE:?}"
                 --testrun-title "${POLARION_RUN_TITLE:?}"
                 --config-map-json "${POLARION_CONFIG_MAP_FILE}"
                 --output "${outputfile}"
                 --submit)


if [[ "${DRY_RUN:-}" == "1" ]]; then
    BEAKER_JOBIDS="J:123456"
fi

# shellcheck disable=SC2206 # we want word splitting
polarion_params+=(${BEAKER_JOBIDS//J:/-j })

if [[ "${DRY_RUN:-}" == "1" ]]; then
    echo "INFO: running $0 as dry run, exiting..."
    echo "python3 main.py" "${polarion_params[@]}"
    popd
    exit 0
fi

echo "INFO: generating polarion xunit results and upload it."
python3 main.py "${polarion_params[@]}"
popd
