#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

. scripts/bkr_common.sh

set -x

function get_nvr_from_advisory()
{
    dnf install -y krb5-workstation # TODO - remove once dependency is included in ztools

    printenv KERNEL_GENERAL_BOT_KEYTAB | base64 --decode > /tmp/kernel_general.keytab
    KRB5CCNAME=$(mktemp) # to avoid `kinit: Invalid UID in persistent keyring name while getting default ccache`
    export KRB5CCNAME
    kinit -k -t /tmp/kernel_general.keytab "${KERNEL_GENERAL_BOT_NAME_REALM:?}"

    # ZErrata may hit ReadTimeout if response not received within 20 seconds, so retry multiple times
    set +e # allow ZErrata to fail without quitting entire script
    local retry=5
    for i in $(seq 1 "${retry}"); do
        TEST_SOURCE_PACKAGE_NVR="$(ZErrata info -e "${ERRATA_ID}" --build)"
        if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
            break
        fi
        echo "WARN: ZErrata failed, sleeping before retry (attempt ${i}/${retry}) ..."
        sleep 60
    done
    set -e # reenable script exit if any cmd fails

    if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
        TEST_SOURCE_PACKAGE_NAME=$(awk -F '-' '{$NF=$(NF-1)=""; print $0}' <<< "${TEST_SOURCE_PACKAGE_NVR}" |\
            xargs | sed 's/ /-/g')
        export TEST_SOURCE_PACKAGE_NVR TEST_SOURCE_PACKAGE_NAME
    fi

    rm -f "${KRB5CCNAME}"
}

# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')

# ztools is provided in the kernel-qe-tools-legacy image;
# ensure it remains up to date for each pipeline run
dnf update -y ztools ztools-config beaker-client beaker-redhat

# If this was an Errata trigger, parse information from the advisory
if [[ -n "${ERRATA_ID:-}" ]]; then
    get_nvr_from_advisory
    if [[ -z "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
        echo "FAIL: could not get source package NVR from advisory ${ERRATA_ID}"
        exit 1
    fi
fi

if [[ -z "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    bkr_get_compose
    distro="${BKR_DISCOVERED_COMPOSE}"
    TEST_SOURCE_PACKAGE_NVR=$(find_compose_pkg -c "${distro}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
    if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
        echo "FAIL: couldn't find nvr for ${TEST_SOURCE_PACKAGE_NAME} on compose ${distro}"
        exit 1
    fi
fi

TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}

# Check if ZSUBMIT_PARAMS is defined - if the array is empty, user still wants
# to execute ZSubmit with default parameters
if [[ -v ZSUBMIT_PARAMS ]]; then
    # allow ZSUBMIT_PARAMS to contain command line execution
    ZSUBMIT_PARAMS=$(eval echo "${ZSUBMIT_PARAMS}")
    # shellcheck disable=SC2312
    readarray -t -d ' ' zsubmit_params < <(echo "${ZSUBMIT_PARAMS}" | tr -d '\n')
    zsubmit_params+=(--nvr "${TEST_PACKAGE_NVR/-debug/}")

    if [[ "${TEST_PACKAGE_NAME}" =~ debug ]]; then
        echo "FAIL: zsubmit jobs shouldn't trigger for debug packages as they are triggered in the same job as for non-debug."
        # https://gitlab.com/redhat/centos-stream/tests/kernel/qe-pipeline-definition/-/issues/46
        exit 1
    fi

    zsubmit_params+=(--arch "${TEST_ARCH}")
    if [[ -n "${ZSUBMIT_TIER:-}" ]]; then
        # shellcheck disable=SC2206 # We want word splitting, tiers are passed as unquoted args
        zsubmit_params+=(--tier ${ZSUBMIT_TIER})
    fi
    if [[ "${DRY_RUN:-}" == "1" ]]; then
        zsubmit_params+=(--dry)
    fi

    ZSubmit "${zsubmit_params[@]}" --idf bkr_ids.txt
fi

if [[ "${DRY_RUN:-}" == "1" ]]; then
    echo "INFO: running as dry run, no job is created."
    echo 'J:123456' | tee bkr_ids.txt
fi

if ! grep -q '[^[:space:]]' bkr_ids.txt; then
    echo "No job created, skipping"
    exit 0
fi

BEAKER_JOBIDS=$(cat bkr_ids.txt)

if [[ -z "${BEAKER_JOBIDS}" ]]; then
    echo "INFO: No job submitted to beaker."
    exit 0
fi

wait_jobs
