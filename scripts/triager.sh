#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

if [[ "${DRY_RUN:-}" == "1" ]]; then
    mv internal/dry-run-output.json ./output.json
fi

if [[ ! -f ./output.json ]]; then
    echo "No result to triage, skipping"
    exit 0
fi

nr_builds=$(jq -r ".builds|length" < output.json)
if [[ "${nr_builds}" -eq "1" ]]; then
    TESTRUN_NAME=$(jq -r ".builds[0].id" < output.json)
    TESTRUN_LOG_URL="https://datawarehouse.cki-project.org/kcidb/builds/${TESTRUN_NAME}"
else
    echo "FAIL: the job shouldn't have submitted more than 1 build"
    exit 1
fi
# these variables are needed by notify_slack.j2
export TESTRUN_LOG_URL
export TESTRUN_NAME

if [[ "${DRY_RUN:-}" != "1" ]]; then
    # This is a temporary solution until we can wait for the submitted results be triaged by DW
    # as ideally we don't want more services to write to DataWarehouse.
    # https://gitlab.com/cki-project/cki-tools/-/issues/188
    if ! CKI_DEPLOYMENT_ENVIRONMENT=production python3 -m cki.triager --from-file ./output.json --to-file ./output_triaged.json --to-dw; then
        echo "FAIL: couldn't submit triaged results to DW. Let's wait for DW to triage it..."
        sleep 30m
    fi
fi

checkoutid=$(jq -r ".checkouts[0].id" < output.json)

success=0
max_attempt=10
attempt=1
while [[ "${success}" -eq 0 ]] && [[ "${attempt}" -le "${max_attempt}" ]]; do
    if [[ "${DRY_RUN:-}" == "1" ]]; then
        mv internal/dry-run-reporter.json ./reporter.json
    else
        # the reporter exits with 1 in case there is untriaged result
        python3 -m reporter -c "${checkoutid}" -b "${TESTRUN_NAME}" -t failure-json > reporter.json || true
    fi
    # check if triager created a valid json file
    if jq -re < reporter.json 1>/dev/null; then
        success=1
    else
        sleep $((60 * attempt ))
        attempt=$(( attempt + 1 ))
    fi
done
if [[ "${success}" -eq 0 ]]; then
    echo "FAIL: couldn't generate output from the reporter"
    exit 1
fi

jinja_renderer -t scripts/notify_slack.j2 -i reporter.json -o slack_message.txt

if [[ ! -f slack_message.txt ]]; then
    echo "FAIL: couldn't create slack message"
    exit 1
fi

if grep "please review" slack_message.txt; then
    TEST_RESULT=failed
    if [[ "${OSCI_GATE_ONLY_FAIL:-}" == "1" ]]; then
        if ! grep "Failed:" slack_message.txt; then
            # Tests maintainers got notified about errored or missed tests,
            # but pipeline was configured to not report gating failure in this case.
            TEST_RESULT=info
        fi
    fi
else
    TEST_RESULT=passed
fi
export TEST_RESULT

if [[ "${REPORT_OSCI:-}" == "1" ]]; then
    OSCI_TEST_STATUS=complete scripts/osci_report_results.sh
fi
