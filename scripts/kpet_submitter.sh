#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

. scripts/bkr_common.sh

set -x
# don't fail pipeline if a command fails
set +e
git clone --depth 1 "${KPET_DB_URL:?}" kpet-db
# remove the / that are passed with TEST_PACKAGE_NAME_ARCH
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')

if [[ -z "${KPET_DESCRIPTION}" ]]; then
    KPET_DESCRIPTION="OS: ${KPET_TREE:?}, arch: ${TEST_ARCH}, test plan: ${KPET_SET:?}"
fi

# allow KPET_DESCRIPTION to contain command line execution
KPET_DESCRIPTION=$(eval echo "${KPET_DESCRIPTION}")

kpet_params=(--tree "${KPET_TREE:?}" --arch "${TEST_ARCH}" --set "${KPET_SET:?}"
             --description "${KPET_DESCRIPTION}" --output beaker.xml)

if [[ "${TEST_PACKAGE_NAME}" =~ kernel-64k ]]; then
    if [[ -z "${KPET_COMPONENT:-}" ]]; then
        KPET_COMPONENT="64kpages"
    else
        KPET_COMPONENT="${KPET_COMPONENT}|64kpages"
    fi
fi

if [[ "${TEST_PACKAGE_NAME}" =~ kernel-rt ]]; then
    if [[ -z "${KPET_COMPONENT:-}" ]]; then
        KPET_COMPONENT="rt"
    else
        KPET_COMPONENT="${KPET_COMPONENT}|rt"
    fi
fi

if [[ "${TEST_PACKAGE_NAME}" =~ debug ]]; then
    if [[ -z "${KPET_COMPONENT:-}" ]]; then
        KPET_COMPONENT="debugbuild"
    else
        KPET_COMPONENT="${KPET_COMPONENT}|debugbuild"
    fi
fi

if [[ -n "${KPET_COMPONENT:-}" ]]; then
    kpet_params+=(--components "${KPET_COMPONENT}")
fi

if [[ -n "${TEST_SOURCE_PACKAGE_NVR:-}" ]]; then
    TEST_PACKAGE_NVR=${TEST_SOURCE_PACKAGE_NVR/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}}

    # Variables are specific to the template used in kpet-db,
    # this variable works on cki kpet-db.
    if [[ "${TEST_SOURCE_PACKAGE_NVR}" =~ kernel ]]; then
        # need to split the nvr to get name, version and release
        # shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
        src_name=$(echo "${TEST_SOURCE_PACKAGE_NVR}" | sed 's/\(.*\)-\(.*\)-\(.*\)/\1/')
        # shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
        version=$(echo "${TEST_SOURCE_PACKAGE_NVR}" | sed 's/\(.*\)-\(.*\)-\(.*\)/\2/')
        # shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
        release=$(echo "${TEST_SOURCE_PACKAGE_NVR}" | sed 's/\(.*\)-\(.*\)-\(.*\)/\3/')

        kpet_params+=(-k "${BREW_TASK_REPOS_URL:?}/official/${src_name}/${version}/${release}/\$basearch#package_name=${TEST_PACKAGE_NAME}&amp;source_package_name=${src_name}")
    else
        kpet_params+=(-v brew_build_nvr="${TEST_SOURCE_PACKAGE_NVR}")
    fi
fi
kpet --db kpet-db run generate "${kpet_params[@]}"
if [[ "${DRY_RUN:-}" == "1" ]]; then
    echo "INFO: running as dry run, no job is submitted."
    echo "['J:123456']" > bkr_job.txt
else
    # CKI adds CKI_MAINTAINERS to the xml job, but kcidb_tool only supports MAINTAINERS
    # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools/-/issues/53
    sed -i -E 's/CKI_MAINTAINERS/MAINTAINERS/' beaker.xml
    bkr_params=(job-submit beaker.xml)
    if [[ -n "${JOB_OWNER:-}" ]]; then
        bkr_params+=(--job-owner "${JOB_OWNER}")
    fi
    bkr "${bkr_params[@]}" > bkr_job.txt
fi
BEAKER_JOBIDS=$(sed -e "s/.*\['\(.*\)'\]/\1/g" < bkr_job.txt)
if [[ -z "${BEAKER_JOBIDS}" ]]; then
    echo "FAIL: Couldn't submit beaker job."
    exit 1
fi

wait_jobs
